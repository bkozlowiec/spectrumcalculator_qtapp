// includes from spctool.cpp
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <stdexcept>
#include <algorithm>
#include <sys/time.h>

#include "spc_interface.h"
#include "qtspcplotter.h"
#include "spcgenerator.h"
#include "libMASSConfigManager.h"
#include "spectra/baseline.h"
#include "inputstructs.h"
#include "gasconfigstructs.h"
#include <QThread>

bool isDateStampPresent = true;

timeval startTime; //Variable used to compute time of fitting
timeval endTime; //Variable used to compute time of fitting

template<class T>
std::ostream & operator<<(std::ostream & str, const std::vector<T> & v)
{
    str << "[";
    for (size_t nn = 0; nn < v.size()-1; nn++)
        str << v[nn] << ", ";
    str << v[v.size()-1] << "]";
    return str;
}

//void convertSpectraFile(std::string currDir);
int paramsAreCorrect(int argc, const std::array<std::string,3>& argv);//(int argc, const char** argv);
void parseCrossStackSpectrumLine(const std::string & line,
        std::vector<double> & outSpectrum,
        double & outTemperature,
        double & outPressure,
        std::string & outDatestamp,
        std::string & outTimestamp);

/**
 * Read stack, flange and EEPROM info from its temporary home in the crossstack
 * INI file.  This should be short-term development stuff.  PCH170301
 */
void readGoofyINI(const std::string & crossStackIniPath,
    StructStackConfig & outStack,
    std::vector<StructStackConfig> & outFlanges,
    StructLaserRampArguments & outEEPROM,
    std::vector<double> & outFlangeT_C,
    std::vector<double> & outFlangeP_torr);

std::string makeOutFileName(const std::string & spectraFilePath, const std::string & suffix);

void printSpectralThings(const SPCInterface * spcInterface);
void printPostFitThings(const SPCInterface* spcInterface);

/**
 *
 * @param argc number of command-line arguments including executable name
 * @param argv argv[1]: specify configuration directory path
 * 				argv[2]: specify spectra file
 * 	usage: spctool conf_O2 spectra/spectra_O2.txt
 *
 * @return
 */
void qtSpcPlotter::emulateSPCtool(std::string fileName, std::string iniDirName)
{
    // Changes to arguments for emulation
    int argc = 3;
    std::array<std::string,3> argv;
    argv[0] = "dummyProgName";
    argv[1] = iniDirName;
    argv[2] = fileName;
    //

    int configOldNew = paramsAreCorrect(argc, argv);

    std::string confDirectory = iniDirName;//confDirectory(argv[1]);
    std::string spectraFilePath = fileName;//spectraFilePath(argv[2]);

    std::vector<double> flangeT_C;
    std::vector<double> flangeP_torr;
    uint64_t fitTime(0);

    //int numLasers = 1;
    //SPCInterface* spcInterface = NULL;
    SPCManager* manager = NULL;
    if (configOldNew < 0)
    {
        exit(1);//return 1;
    }
    else if (configOldNew == 0) // Old way with peaks.ini, spc_config, etc...
    {
        //spcInterface = SPCFactory::createInterface();
        //spcInterface->initSPC(confDirectory, numLasers);
    }
    else if (configOldNew == 1) // new way with crossstack.ini
    {
        StructLaserRampArguments eeprom;
        StructStackConfig stack;
        std::vector<StructStackConfig> flanges;
        std::vector<StructError> lmErrors;
        std::string sVersion;

        readGoofyINI(confDirectory + "/crossstack.ini", stack, flanges, eeprom, flangeT_C, flangeP_torr);
//        flangeT_C.assign(flanges.size(), safeT_C);
//        flangeP_torr.assign(flanges.size(), safeP_torr);

        manager = SPCFactory::createManager(confDirectory, "O2_760NM");
        sVersion = manager->getVersion();
        lmErrors = manager->getErrors();
        manager->initStack(stack);
        manager->initFlanges(flanges);
        manager->initLaserRampArguments(eeprom);
        manager->setMode(SPCInterface::kDefaultMode);
        manager->getInterface()->setDoCalculateInitialY(0,true); ///////////////////////////////////////////////
        manager->getInterface()->setDoCalculateEstimatedY(0,true); /////////////////////////////////////////////

        std::cout << "INI version is " << sVersion << std::endl;
        for (std::size_t i = 0; i<lmErrors.size(); i++)
        {
        std::cout << "Error Code, LogIt, Log Message: "
                << lmErrors[i].errorCode << " " << std::boolalpha << lmErrors[i].logToJournal << " \""
                << lmErrors[i].logMessage << "\"" << std::endl;
        }

        // This pointer will go out of date whenever we change things with the
        // manager
        //spcInterface = manager->getInterface();
    }
    else
    {
        std::cout << "Unknown Configuration Mode: " << configOldNew << std::endl;
        exit(0);//return 0;
    }

    //// Addition to get the input (read from crossstack.ini) baseline parameters
    std::vector<double> inputBaselineParams;
    LibMASSConfigManager* castedManager = dynamic_cast<LibMASSConfigManager*>(manager);

    if( castedManager )
        inputBaselineParams = castedManager->getPeaksINI().baselineParameters;
    ////

    // Analysis, output, blah
    std::ofstream fout(makeOutFileName(spectraFilePath, "res.txt"));
    if (false == fout.is_open())
        throw std::runtime_error("Output file did not open.");

    // Plot, output, blah
    std::ofstream fplotout(makeOutFileName(spectraFilePath, "res_plot.txt"));
    if (false == fplotout.is_open())
        throw std::runtime_error("Output plot file did not open.");

    //now read the spectra and compute the spectrum
    std::ifstream spectraFile(spectraFilePath.c_str());
    if (false == spectraFile.is_open())
        throw std::runtime_error("Spectra file did not open.");

    std::string lineFromSpectraFile;
    getline(spectraFile, lineFromSpectraFile); // read and ignore header line

    //check which log file format from header
    if(std::string::npos == lineFromSpectraFile.find("Date"))
    {
        isDateStampPresent = false;
    }

    unsigned int spectraID = 0;
    double temperature, P_torr;
    std::vector<int> ringdownCount;
    std::string datestamp;
    std::string timestamp;

    std::vector<double>* spectrumPtr; /////////////////////////////////////////////////////////////////////////////////

    bool isFirstPass = true; // because spc interface makes it hard to write header before one fit is done
    while (getline(spectraFile, lineFromSpectraFile))
    {
        // Cull empty lines and comment lines
        if (lineFromSpectraFile.length() <= 1)
            continue;
        if (lineFromSpectraFile.at(0) == '#')
            continue;

        static std::vector<double> spectrum;
        parseCrossStackSpectrumLine(lineFromSpectraFile, spectrum, temperature, P_torr, datestamp, timestamp);
        spectraID++;

        spectrumPtr = &spectrum;

        ringdownCount.push_back(manager->getInterface()->getRDFitCnt());

        unsigned int iRampStart = manager->getInterface()->getLaserRampStartIdx();
        unsigned int iRampEnd = iRampStart + manager->getInterface()->getLaserRampLength();

        unsigned int iRingdownStart = manager->getInterface()->getLaserOFFStartIdx();
        unsigned int iRingdownEnd = iRingdownStart + manager->getInterface()->getLaserOFFLength();

//        std::vector<double> ghz = spcInterface->getGHzPts(0);
//        std::cout << "GHz: " << ghz.size() << " points from " << ghz[0] << " to " << ghz[ghz.size()-1] << "\n";

//        std::cout << "#ramp " << iRampStart << " " << iRampEnd << " ringdown "
//                << iRingdownStart << " " << iRingdownEnd << "\n";

        std::vector<double> laserRampData(&(spectrum.at(iRampStart)),
            &(spectrum.at(iRampEnd-1)) + 1);
        std::vector<double> ringdownData(&spectrum.at(iRingdownStart),
            &spectrum.at(iRingdownEnd-1) + 1);

        // Enable linelock
        manager->getInterface()->setEnableLinelock(0, true);

        gettimeofday(&startTime, NULL);

        manager->getInterface()->computeSpectrum(
            temperature,
            P_torr,
            flangeT_C,
            flangeP_torr,
            std::vector<std::vector<double> >(1, laserRampData),
            std::vector<std::vector<double> >(1, ringdownData),
            ringdownCount);

        gettimeofday(&endTime, NULL);

        fitTime = ((endTime.tv_sec - startTime.tv_sec)*1000 +
                                        (endTime.tv_usec - startTime.tv_usec)/1000);

        std::cout << std::endl << "Fit time: " << fitTime <<std::endl;
//    std::cout << "data = " << spectrum << "\n";

        bool printThings = false;
        if (printThings)
        {
            printSpectralThings(manager->getInterface());
        }

        std::vector<double> baselineParams = manager->getInterface()->getBaselineParameters(0);
        std::vector<double> peakParams = manager->getInterface()->getPeakParameters(0);
        double deltaTEC = manager->getInterface()->getLinelockDeltaTEC(0);

        std::cout << baselineParams.size() << " baseline params, "
                << peakParams.size() << " peak params\n";

        std::map<std::string, double> engineeringValues = manager->getInterface()->getEVs();

        if (isFirstPass)
        {
            isFirstPass = false;
            if (isDateStampPresent)
            {
                fout << "Date\tTime\t";
            }
            else
            {
                fout << "Time\t";
            }
            std::map<std::string, double>::iterator itr;
            for (itr = engineeringValues.begin(); itr != engineeringValues.end(); itr++)
                fout << itr->first << "\tConc\t";

            fout << "T\tP\tGoF\tPower\tEtalonStretch\ta0\ta1\ta2\ta3\t";
            for (std::size_t nn = 0; nn < peakParams.size()/4; nn++)
            {
                fout << "center"<< nn <<"\tarea" << nn << "\tdw" << nn << "\tpw" << nn << "\t";
            }
            fout << std::endl;
        }
        //Output to monitor progress D.S. 12/29/2015
        double goodnessOfFit = manager->getInterface()->getGoodnessOfFit(0);
        double laserPower = manager->getInterface()->getLaserPowerArbUnits(0);
        double EtalonStretch = manager->getInterface()->getEtalonStretchFactor(0);
        double PeakZeroShiftGHz = manager->getInterface()->getPeakZeroShiftGHz(0);
        std::cout << "ID = " << spectraID << std::endl;
        std::cout << "Temperature = " << temperature << " C" << std::endl;
        std::cout << "Pressure = " << P_torr / 750.062 << " bar" << std::endl;
        std::cout << "Goodness of Fit = " << goodnessOfFit << std::endl;
        std::cout << "DeltaTEC = " << deltaTEC << std::endl;
        std::cout << "Laser Power = " << laserPower << std::endl;
        std::cout << "Etalon Stretch = " << EtalonStretch << std::endl;
        std::cout << "Zero Peak Shift = " << PeakZeroShiftGHz << " GHz" << std::endl;

        if (isDateStampPresent)
        {
            fout << datestamp << "\t" << timestamp << "\t";
        }
        else
        {
            fout << timestamp << "\t";
        }

        std::map<std::string, double>::iterator itr;
        for (itr = engineeringValues.begin(); itr != engineeringValues.end(); itr++)
        {
            fout << itr->second << "\t" << itr->second/1e6 << "\t";
            std::cout << "Concentration " << itr->first << " = " << (itr->second)*0.0001 << "%" << std::endl; //Output to monitor progress D.S. 12/29/2015
        }
        fout << temperature << "\t" << P_torr / 750.062 << "\t" << goodnessOfFit << "\t" << laserPower << "\t" << EtalonStretch << "\t";
        for (unsigned int ii = 0; ii < baselineParams.size(); ii++)
            fout << baselineParams[ii] << "\t";
        for (unsigned int ii = 0; ii < peakParams.size(); ii++)
            fout << peakParams[ii] << "\t";
        fout << std::endl;

        // Output Plot Data
        std::vector<double> measuredData = manager->getInterface()->getMeasuredData(0);
        std::vector<double> peakFit = manager->getInterface()->getPeakFit(0);
        std::vector<double> ghzPts = manager->getInterface()->getGHzPts(0);
        if (isDateStampPresent)
        {
            fplotout << datestamp << "\t" << timestamp << "\t";
        }
        else
        {
            fplotout << "\t" << timestamp << "\t";
        }
        for (itr = engineeringValues.begin(); itr != engineeringValues.end(); itr++)
        {
            fplotout << itr->second << "\t" << itr->second / 1e6 << "\t" << temperature << "\t" << P_torr / 750.062 << "\t";
        }
        fplotout << goodnessOfFit << "\t" << laserPower << "\t";
        for (unsigned int ii = 0; ii < measuredData.size(); ii++)
            fplotout << measuredData[ii] << "\t";
        for (unsigned int ii = 0; ii < peakFit.size(); ii++)
            fplotout << peakFit[ii] << "\t";
        fplotout << std::endl;


        if (printThings)
        {
            printPostFitThings(manager->getInterface());
        }


    }
    fout.close();
    fplotout.close();
    spectraFile.close();

    // Additional output for plotting in SpectrumPlotter (B. Kozlowiec)
    ghzPoints       = manager->getInterface()->getGHzPts(0);
    rawSignal       = manager->getInterface()->getRawSignal(0);
    baselineFit     = manager->getInterface()->getBaselineFit(0);
    measuredData    = manager->getInterface()->getMeasuredDataWithBL(0);
    peakFit         = manager->getInterface()->getPeakFitWithBL(0);
    initialGuessFit = manager->getInterface()->getInitialGuessFitWithBL(0);
    estimatedFit    = manager->getInterface()->getEstimatedFitWithBL(0);
    originalSig     = *spectrumPtr;

    // Some additional output to obtain the InitialGuess spectrum computed outside the libMASS
    oldBlParams = inputBaselineParams;
    newBlParams = manager->getInterface()->getBaselineParameters(0);

    // Output for comparing Initial Guess With BL vs "manually-produced" spectrum
    manualInitGuess = manager->getInterface()->getInitialGuessFit(0);
    for(unsigned i=0; i<manualInitGuess.size(); i++)
        manualInitGuess[i] += (oldBlParams[0] + oldBlParams[1] * ghzPoints[i]);

    NumOfPoints = ghzPoints.size();

    std::cout << "Num of IG fit points: " << manager->getInterface()->getInitialGuessFit(0).size() << std::endl;
    std::cout << "Num of RS points: " << manager->getInterface()->getRawSignal(0).size() << std::endl;
    std::cout << "Num of PF points: " << manager->getInterface()->getPeakFit(0).size() << std::endl;
    //std::vector<double>::iterator max = std::max_element(manager->getInterface()->getPeakFit(0).begin(),manager->getInterface()->getPeakFit(0).end());
    //std::vector<double>::iterator min = std::min_element(manager->getInterface()->getPeakFit(0).begin(),manager->getInterface()->getPeakFit(0).end());
    //std::cout << "PF min and max: " << *min << " " << *max << std::endl;
    std::cout << "Num of MD points: " << manager->getInterface()->getMeasuredData(0).size() << std::endl;
}

////////////// An additional member function to obtain the InitialGuess spectrum computed outside the libMASS (but with offset) ///////////////

std::vector<double> qtSpcPlotter::computeMyInitGuess()
{
    std::vector<double> workerVec;
    std::vector<double> returnVec;

    for(unsigned i=0; i<baselineFit.size(); i++)
        workerVec.push_back( baselineFit[i] - initialGuessFit[i]*baselineFit[i] );

    std::vector<double> origBaseline;
    Baseline origBaselineObj;
    origBaselineObj.setParams(oldBlParams);

    for(unsigned i=0; i<baselineFit.size(); i++)
        origBaseline.push_back( origBaselineObj.f(ghzPoints[i],0) );

    for(unsigned i=0; i<baselineFit.size(); i++)
        returnVec.push_back( (origBaseline[i] - workerVec[i])/origBaseline[i] );

    return returnVec;
}

/*********************************************** Global functions from spctool.cpp ***************************************************/


int paramsAreCorrect(int argc, const std::array<std::string,3>& argv) //(int argc, const char** argv)
{
    std::string path1, path2;
    int exitCode = -1;
    if (argc == 3)
    {
        //check if arguments are valid
        //if configuration directory is valid
        std::string path = argv[1];
        path1 = path + "/" + "spc_config.ini";
        std::ifstream fileCheck1(path1.c_str());
        path2 = path + "/" + "crossstack.ini";
        std::ifstream fileCheck2(path2.c_str());
        if (fileCheck1.good())
        {
            // Old mode with peaks.ini, spc_config.ini, etc...
            exitCode = 0;
            fileCheck1.close();
        }
        else if (fileCheck2.good())
        {
            // New mode with crossstack.ini
            exitCode = 1;
            fileCheck2.close();
        }
        else
        {
            exitCode = -1;
            std::cout << "Configuration directory: "
                << argv[1]
                << " is either not valid or it does not contain valid .INI file(s)."
                << std::endl;
        }

        //if spectra file is valid
        path1 = argv[2];
        fileCheck1.open(path1.c_str());
        if (fileCheck1.good())
        {
            fileCheck1.close();
        }
        else
        {
            exitCode = -1;
            std::cout << "Spectra File: " << argv[2] << " does not exist" << std::endl;
        }
    }
    else
    {
        std::cout << "Usage " << std::endl;
        std::cout << "  ./spctool <configuration directory> <spectra file>" << std::endl;
        std::cout << "  e.g. " << std::endl;
        std::cout << "  ./spctool ./conf_O2 ./spectra/spectra_O2.txt" << std::endl;
        exitCode = -1;
    }

    return exitCode;
}

void parseCrossStackSpectrumLine(const std::string & line,
    std::vector<double> & outSpectrum,
    double & outTemperature,
    double & outPressure,
    std::string & outDatestamp,
    std::string & outTimestamp)
{
    int nOfPoints;
    outSpectrum.clear();
    outSpectrum.reserve(4000);

    std::stringstream dataStream(line);
    std::string dataPoint(""); //string to store a data point from logline

    //extract individual points from the line
    /* Log file format with column name as below
     *  Acq_Num
     *  Date
     *  Timestamp
     *  Pressure
     *  Temperature
     *  TEC_Temp
     *  16 Values
     *  EV1
     *  EV2
     *  EV3
     *  EV4
     *  EV5
     *  EV6
     *  EV7
     *  EV8
     *  Auxiliary 1
     *  Auxiliary 2
     *  Data_len
     *  DP1
     *  <column with heading DP<n>, where n is integer, will continue for Data_len
    */
    std::string value("");
    dataStream >> value; // acquisition number
    if (value == "Acq_Num")
        throw std::runtime_error("Trying to parse header line, stop that!");

    if (isDateStampPresent)
    {
        dataStream >> outDatestamp; // Datestamp
    }
    dataStream >> outTimestamp; // Timestamp

    dataStream >> outPressure;
    if (!isDateStampPresent)
    {
        outPressure *= 0.001; // Convert bar to mbar
    }
    outPressure *= 750.062; // convert pressure from BAR to TORR

    dataStream >> outTemperature;

    dataStream >> value; // TEC temperature

    for(unsigned int diagIdx = 0; diagIdx < 16; diagIdx++)
    {
        dataStream >> value;
    }

    // advance the stream past the engineering values (EVs)
    for(unsigned int evIdx = 0; evIdx < 8; evIdx++)
        dataStream >> value;

    //logs with device status does not has Auxiliary
    dataStream >> value;
    dataStream >> value;
    // Number of data points
    dataStream >> nOfPoints;

    //now get all data points of the spectrum stored in a line
    double spectrumDataPoint;
    for (int i = 0; i< nOfPoints; i++)
    {
        dataStream >> spectrumDataPoint;
        outSpectrum.push_back(spectrumDataPoint);
    }

    /*
        Cross Stack log support different number of data point
        per spectrum which is configured by the TX settings of Laser
        This code currently required 4000 data points which is the
        max number of data points supported by Cross Stack.
        This code disect Laser Ramp and Ringdown region from spectrum
        based on the index value of LASER_RAMP_START and LASER_OFF_START
        variables in spc_config.ini file respectively.

        TODO code needs to modify based on the section ratio of
        laser Ramp and ringdown in a spectrum and not on fixed index value
        So as to accomodate different number of data points in spectrum.

        Since we have fixed start index so tempIntVec needs to be filled with
        0 in case data points are less than 4000
    */

    // shouldn't need this line though!
    if (outSpectrum.size() < 4000)
        outSpectrum.resize(4000, 0.0);

//    std::vector<double> dataPts(&spectrum[lrStartIdx], &spectrum[lrStartIdx + lrLength - 1]);
//    std::vector<double> dataPts2(&spectrum[rdStartIdx], &spectrum[rdStartIdx + rdLength - 1]);
//
//    std::cout << std::endl <<"dataPts.size() " << dataPts.size();
//    std::cout << std::endl <<"dataPts2.size() " << dataPts2.size();
//
//    laserRamp.push_back(dataPts);
//    rdData.push_back(dataPts2);
//    spectraID++;

}

void readGoofyINI(const std::string & crossStackIniPath,
    StructStackConfig & outStack,
    std::vector<StructStackConfig> & outFlanges,
    StructLaserRampArguments & outEEPROM,
    std::vector<double> & outFlangeT_C,
    std::vector<double> & outFlangeP_torr)
{
    std::string lineFromFile, key, token; // A line has a key often followed by some tokens

    outStack.absorbers.clear();
    outFlanges.clear();

    std::stringstream sstr("");
    std::ifstream crossstackFile;
    crossstackFile.open(crossStackIniPath.c_str(), std::ios::in);

    while (getline(crossstackFile, lineFromFile))
    {
        if (lineFromFile.length() < 1 || lineFromFile.at(0) == '#')
        {
            // Skip blank lines or comments (lines starting with #)
        }
        else
        {
            std::istringstream ss(lineFromFile);
            // Read a key and convert to uppercase
            key = "";
            ss >> key;
            std::transform(key.begin(), key.end(), key.begin(), ::toupper);

            // Has to come from EEPROM
            if (key == "ETALON_COEFFICIENTS")
            {
                outEEPROM.etalonCoeffients.assign(5, 0.0);
                ss >> outEEPROM.etalonCoeffients[0];
                ss >> outEEPROM.etalonCoeffients[1];
                ss >> outEEPROM.etalonCoeffients[2];
                ss >> outEEPROM.etalonCoeffients[3];
                ss >> outEEPROM.etalonCoeffients[4];
            }
            // Has to come from EEPROM
            if (key == "SHIFT")
            {
                ss >> outEEPROM.shift_GHz;
            }
            // Has to come from EEPROM
            if (key == "BL_SAMPLE")
            {
                outEEPROM.baselineSample.assign(4, 0);
                ss >> outEEPROM.baselineSample[0];
                ss >> outEEPROM.baselineSample[1];
                ss >> outEEPROM.baselineSample[2];
                ss >> outEEPROM.baselineSample[3];
            }
            // Has to come from EEPROM
            if (key == "FIT_WINDOW")
            {
                outEEPROM.fitWindow.assign(2, 0);
                ss >> outEEPROM.fitWindow[0];
                ss >> outEEPROM.fitWindow[1];
            }
            if (key == "LASER_RAMP_START")
            {
                ss >> outEEPROM.laserRampStart;
            }
            if (key == "LASER_RAMP_LENGTH")
            {
                ss >> outEEPROM.laserRampLength;
            }
            if (key == "LASER_OFF_START")
            {
                ss >> outEEPROM.laserOffStart;
            }
            if (key == "LASER_OFF_LENGTH")
            {
                ss >> outEEPROM.laserOffLength;
            }
            // Has to come from APPS
            if (key == "STACK_PROPERTIES")
            {
                double length_m;
                ss >> length_m;
                outStack.length_m = length_m;
            }
            if (key == "FLANGE_PROPERTIES")
            {
                double length_m, flangeT_C, flangeP_bar;
                int flangeIndex;
                ss >> flangeIndex >> length_m >> flangeT_C >> flangeP_bar;

                if (flangeIndex >= (int)outFlanges.size())
                {
                    // this is why flanges should be in order.
                    // Need to push a new flange, so keep the flange params and T and P and all that
                    StructStackConfig strFlangeParams;
                    strFlangeParams.length_m = length_m;
                    outFlanges.push_back(strFlangeParams);

                    outFlangeT_C.push_back(flangeT_C);
                    outFlangeP_torr.push_back(flangeP_bar*750.062);
                }
                else
                {
                    outFlanges.at(flangeIndex).length_m = length_m;
                    outFlangeT_C.at(flangeIndex) = flangeT_C;
                    outFlangeP_torr.at(flangeIndex) = flangeP_bar*750.062;
                }
            }
            if (key == "STACK_GAS")
            {
                std::string gas, ind_gas;
                double init_conc, ind_coeff;
                ss >> gas >> init_conc;
                //outStack.length_m = length;

                StructAbsorber strAbsorber;
                strAbsorber.moleculeName = gas;
                strAbsorber.concentration_ppm = init_conc;
                while (ss >> ind_gas >> ind_coeff)
                {
                    strAbsorber.broadeningCoefficents.push_back({ind_gas, ind_coeff});
                }
                outStack.absorbers.push_back(strAbsorber);
            }
            if (key == "FLANGE_GAS")
            {
                std::string gas, ind_gas;
                int flangeIndex;
                double init_conc, ind_coeff;
                ss >> flangeIndex >> gas >> init_conc;

                StructAbsorber strAbsorber;
                strAbsorber.moleculeName = gas;
                strAbsorber.concentration_ppm = init_conc;
                while (ss >> ind_gas >> ind_coeff)
                {
                    strAbsorber.broadeningCoefficents.push_back({ ind_gas, ind_coeff });
                }

                if (flangeIndex >= (int)outFlanges.size())
                {
                    // Need to push a new flange, so keep the flange params and T and P and all that
                    StructStackConfig strFlangeParams;
                    strFlangeParams.length_m = -1.0;
                    strFlangeParams.absorbers.push_back(strAbsorber);

                    outFlanges.push_back(strFlangeParams);
                    outFlangeT_C.push_back(0); // just... because... this works
                    outFlangeP_torr.push_back(0);
                }
                else
                {
                    // Just need to push the flange gas's affected gases and broadening factor.
                    outFlanges[flangeIndex].absorbers.push_back(strAbsorber);
                }
            }
            /*
            if (key == "CELSIUS")
            {
                ss >> safeT_C;
            }
            if (key == "TORR")
            {
                ss >> safeP_torr;
            }
            */
        }
    }
    crossstackFile.close();
}

void readGoofyINI(const std::string & crossStackIniPath,
    StructLaserRampArguments & outEEPROM)
{
    std::string lineFromFile, key, token; // A line has a key often followed by some tokens

    std::stringstream sstr("");
    std::ifstream crossstackFile;
    crossstackFile.open(crossStackIniPath.c_str(), std::ios::in);

    while (getline(crossstackFile, lineFromFile))
    {
        if (lineFromFile.length() < 1 || lineFromFile.at(0) == '#')
        {
            // Skip blank lines or comments (lines starting with #)
        }
        else
        {
            std::istringstream ss(lineFromFile);
            // Read a key and convert to uppercase
            key = "";
            ss >> key;
            std::transform(key.begin(), key.end(), key.begin(), ::toupper);

            // Has to come from EEPROM
            if (key == "ETALON_COEFFICIENTS")
            {
                outEEPROM.etalonCoeffients.assign(5, 0.0);
                ss >> outEEPROM.etalonCoeffients[0];
                ss >> outEEPROM.etalonCoeffients[1];
                ss >> outEEPROM.etalonCoeffients[2];
                ss >> outEEPROM.etalonCoeffients[3];
                ss >> outEEPROM.etalonCoeffients[4];
            }
            // Has to come from EEPROM
            if (key == "SHIFT")
            {
                ss >> outEEPROM.shift_GHz;
            }
            // Has to come from EEPROM
            if (key == "BL_SAMPLE")
            {
                outEEPROM.baselineSample.assign(4, 0);
                ss >> outEEPROM.baselineSample[0];
                ss >> outEEPROM.baselineSample[1];
                ss >> outEEPROM.baselineSample[2];
                ss >> outEEPROM.baselineSample[3];
            }
            // Has to come from EEPROM
            if (key == "FIT_WINDOW")
            {
                outEEPROM.fitWindow.assign(2, 0);
                ss >> outEEPROM.fitWindow[0];
                ss >> outEEPROM.fitWindow[1];
            }
            if (key == "LASER_RAMP_START")
            {
                ss >> outEEPROM.laserRampStart;
            }
            if (key == "LASER_RAMP_LENGTH")
            {
                ss >> outEEPROM.laserRampLength;
            }
            if (key == "LASER_OFF_START")
            {
                ss >> outEEPROM.laserOffStart;
            }
            if (key == "LASER_OFF_LENGTH")
            {
                ss >> outEEPROM.laserOffLength;
            }
        }
    }
    crossstackFile.close();
}

void readStackInputs(const std::string& broadeningCoeffsIniPath,
    StructStackConfig & outStack,
    double& temperature,
    double& P_torr,
    const GASCONFIG_SIM_FIT_STRUCT& appInputs,
    bool simOrFit )
{
    std::string lineFromFile;

    outStack.absorbers.clear();

    // read from inputsstruct and AllBroadeningCoeffs.ini
    std::ifstream broadeningCoeffsFile;
    broadeningCoeffsFile.open(broadeningCoeffsIniPath.c_str(), std::ios::in);
    double ind_coeff12, ind_coeff21, ind_coeff31, ind_coeff32;
    std::string gasName1, gasName2, gasName3;

    if( !broadeningCoeffsFile.is_open() )
    {
        std::cerr << "AllBroadeningCoefficients.ini not found!" << std::endl;
        return;
    }

    // Read data for simulation or for fitting?
    const GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT* appInputsPtr;
    if( simOrFit == true ) // == sim
        appInputsPtr = &appInputs.simData;
    else // == fit
        appInputsPtr = &appInputs.fitData;

    // STACK part
    outStack.length_m = appInputsPtr->stack.L / 1000;
    temperature = appInputsPtr->stack.T;
    P_torr      = bar2torr( appInputsPtr->stack.P );

    StructAbsorber strAbsorber1;
    strAbsorber1.moleculeName = appInputs.nameX1;
    strAbsorber1.concentration_ppm = appInputsPtr->stack.X1;

    StructAbsorber strAbsorber2;
    strAbsorber2.moleculeName = appInputs.nameX2;
    strAbsorber2.concentration_ppm = appInputsPtr->stack.X2;

    // Ignore first two lines of INI file
    getline(broadeningCoeffsFile, lineFromFile);
    getline(broadeningCoeffsFile, lineFromFile);

    bool loopErrorStatus = true;
    while( getline(broadeningCoeffsFile, lineFromFile) )
    {
        std::istringstream ss(lineFromFile);
        gasName1.clear();
        gasName2.clear();

        ss >> gasName1;

        if( gasName1 == strAbsorber1.moleculeName )
        {
            ss >> gasName2;
            if( gasName2 == strAbsorber2.moleculeName )
            {
                ss >> ind_coeff12;
                continue;
            }
            else
            {
                ss.ignore(128,'\t');
                ss >> gasName2;
                if( gasName2 == strAbsorber2.moleculeName )
                {
                    ss >> ind_coeff12;
                    continue;
                }
                else
                    std::cerr << "Error! Broadening coefficient for gases " << strAbsorber1.moleculeName
                              << " and " << strAbsorber2.moleculeName << " not found." << std::endl;
            }
        }
        else if( gasName1 == strAbsorber2.moleculeName )
        {
            ss >> gasName2;
            if( gasName2 == strAbsorber1.moleculeName )
            {
                ss >> ind_coeff21;
                continue;
            }
            else
            {
                ss.ignore(128,'\t');
                ss >> gasName2;
                if( gasName2 == strAbsorber1.moleculeName )
                {
                    ss >> ind_coeff21;
                    continue;
                }
                else
                    std::cerr << "Error! Broadening coefficient for gases " << strAbsorber2.moleculeName
                              << " and " << strAbsorber1.moleculeName << " not found." << std::endl;
            }
        }
        loopErrorStatus = false;
        break;
    }
    if( loopErrorStatus )
        std::cerr << "Error! Name of one of gases: " << strAbsorber1.moleculeName << ", " << strAbsorber2.moleculeName
                  << " not found in AllBroadeningCoefficients.ini!" << std::endl;

    strAbsorber1.broadeningCoefficents.push_back({appInputs.nameX1, ind_coeff12}); // To be created from broadening coeffs struct (?)
    outStack.absorbers.push_back(strAbsorber1);
    strAbsorber2.broadeningCoefficents.push_back({appInputs.nameX2, ind_coeff21});
    outStack.absorbers.push_back(strAbsorber2);

    // Input for additional gases (matrix components)
    if( appInputsPtr->stack.matrX.size() )
    {
        std::vector<StructAbsorber> strAbsorbersVec(appInputsPtr->stack.matrX.size());

        for(unsigned i=0; i<strAbsorbersVec.size(); i++)
        {
            bool loopErrorStatus = false;
            strAbsorbersVec[i].moleculeName = appInputsPtr->stack.nameMatrX[i];
            strAbsorbersVec[i].concentration_ppm = appInputsPtr->stack.matrX[i];

            broadeningCoeffsFile.clear();
            broadeningCoeffsFile.seekg(0, ios::beg);

            // Ignore first two lines of INI file
            getline(broadeningCoeffsFile, lineFromFile);
            getline(broadeningCoeffsFile, lineFromFile);

            // broadening...
            while( getline(broadeningCoeffsFile, lineFromFile) )
            {
                loopErrorStatus = false;

                std::istringstream ss(lineFromFile);
                gasName1.clear();
                gasName2.clear();
                gasName3.clear();

                ss >> gasName3;

                if( gasName3 == strAbsorbersVec[i].moleculeName )
                {
                    ss >> gasName1;

                    if( gasName1 == strAbsorber1.moleculeName )
                        ss >> ind_coeff31;
                    else if( gasName1 == strAbsorber2.moleculeName )
                        ss >> ind_coeff32;
                    else
                        std::cerr << "Error! Broadening coefficient for mixture of " << strAbsorbersVec[i].moleculeName
                                  << " with " << strAbsorber1.moleculeName << " and " << strAbsorber2.moleculeName
                                  << " not found." << std::endl;

                    ss.ignore(128,'\t');
                    ss >> gasName2;

                    if( gasName2 == strAbsorber1.moleculeName )
                        ss >> ind_coeff31;
                    else if( gasName2 == strAbsorber2.moleculeName )
                        ss >> ind_coeff32;
                    else
                        std::cerr << "Error! Broadening coefficient for mixture of " << strAbsorbersVec[i].moleculeName
                                  << " with " << strAbsorber1.moleculeName << " and " << strAbsorber2.moleculeName
                                  << " not found." << std::endl;

                    break;
                }

                loopErrorStatus = true;
            }

            if( loopErrorStatus )
                std::cerr << "Error! Name of gas: " << strAbsorbersVec[i].moleculeName
                          << " not found in AllBroadeningCoefficients.ini!" << std::endl;

            strAbsorbersVec[i].broadeningCoefficents.push_back({strAbsorber1.moleculeName, ind_coeff31}); // To be created from broadening coeffs struct (?)
            strAbsorbersVec[i].broadeningCoefficents.push_back({strAbsorber2.moleculeName, ind_coeff32});
            outStack.absorbers.push_back(strAbsorbersVec[i]);
        }
    }
    broadeningCoeffsFile.close();
}

void readFlangesInputs(const std::string& broadeningCoeffsIniPath,
    std::vector<StructStackConfig> & outFlanges,
    std::vector<double> & outFlangeT_C,
    std::vector<double> & outFlangeP_torr,
    const GASCONFIG_SIM_FIT_STRUCT& appInputs,
    bool simOrFit)
{
    std::string lineFromFile;

    outFlanges.clear();

    // read from inputsstruct and AllBroadeningCoeffs.ini
    std::ifstream broadeningCoeffsFile;
    broadeningCoeffsFile.open(broadeningCoeffsIniPath.c_str(), std::ios::in);
    double ind_coeff12, ind_coeff21, ind_coeff31, ind_coeff32;
    std::string gasName1, gasName2, gasName3;

    if( !broadeningCoeffsFile.is_open() )
    {
        std::cerr << "AllBroadeningCoefficients.ini not found!" << std::endl;
        return;
    }

    // Read data for simulation or for fitting?
    const GASCONFIG_SIM_FIT_STRUCT::GASCONFIG_SIM_FIT_INPUT* appInputsPtr;
    if( simOrFit == true ) // == sim
        appInputsPtr = &appInputs.simData;
    else // == fit
        appInputsPtr = &appInputs.fitData;

    for(unsigned i=0; i<appInputsPtr->flanges.size(); i++)
    {
        outFlanges.push_back(StructStackConfig());
        outFlangeT_C.push_back(appInputsPtr->flanges[i].T);
        outFlangeP_torr.push_back( bar2torr(appInputsPtr->flanges[i].P) );
    }

    for( unsigned i=0; i<outFlanges.size(); i++ )
    {
        StructAbsorber flStrAbsorber1;
        StructAbsorber flStrAbsorber2;

        std::vector<std::string> flMatxGases;
        std::vector<double>      flMatxConcs;

        outFlanges[i].length_m = appInputsPtr->flanges[i].L / 1000;

        flStrAbsorber1.moleculeName = appInputs.nameX1;
        flStrAbsorber1.concentration_ppm = appInputsPtr->flanges[i].X1;

        flStrAbsorber2.moleculeName = appInputs.nameX2;
        flStrAbsorber2.concentration_ppm = appInputsPtr->flanges[i].X2;

        // Input for additional flange gases (matrix components - set to 2 in INPUT_VARIABLES_STRUCT)
        // Creating artificial vectors for consistency and in case of input structures modifications
        flMatxGases = appInputsPtr->flanges[i].nameMatrX;
        flMatxConcs = appInputsPtr->flanges[i].matrX;

        broadeningCoeffsFile.clear();
        broadeningCoeffsFile.seekg(0, ios::beg);

        // Ignore first two lines of INI file
        getline(broadeningCoeffsFile, lineFromFile);
        getline(broadeningCoeffsFile, lineFromFile);

        bool flLoopErrorStatus = true;
        while( getline(broadeningCoeffsFile, lineFromFile) )
        {
            std::istringstream ss(lineFromFile);
            gasName1.clear();
            gasName2.clear();

            ss >> gasName1;

            if( gasName1 == flStrAbsorber1.moleculeName )
            {
                ss >> gasName2;
                if( gasName2 == flStrAbsorber2.moleculeName )
                {
                    ss >> ind_coeff12;
                    continue;
                }
                else
                {
                    ss.ignore(128,'\t');
                    ss >> gasName2;
                    if( gasName2 == flStrAbsorber2.moleculeName )
                    {
                        ss >> ind_coeff12;
                        continue;
                    }
                    else
                        std::cerr << "Error! Broadening coefficient for gases " << flStrAbsorber1.moleculeName
                                  << " and " << flStrAbsorber2.moleculeName << " not found." << std::endl;
                }
            }
            else if( gasName1 == flStrAbsorber2.moleculeName )
            {
                ss >> gasName2;
                if( gasName2 == flStrAbsorber1.moleculeName )
                {
                    ss >> ind_coeff21;
                    continue;
                }
                else
                {
                    ss.ignore(128,'\t');
                    ss >> gasName2;
                    if( gasName2 == flStrAbsorber1.moleculeName )
                    {
                        ss >> ind_coeff21;
                        continue;
                    }
                    else
                        std::cerr << "Error! Broadening coefficient for gases " << flStrAbsorber2.moleculeName
                                  << " and " << flStrAbsorber1.moleculeName << " not found." << std::endl;
                }
            }
            flLoopErrorStatus = false;
            break;
        }
        if( flLoopErrorStatus )
            std::cerr << "Error! Name of one of gases: " << flStrAbsorber1.moleculeName << ", " << flStrAbsorber2.moleculeName
                      << " not found in AllBroadeningCoefficients.ini!" << std::endl;

        flStrAbsorber1.broadeningCoefficents.push_back({flStrAbsorber2.moleculeName, ind_coeff12}); // To be created from broadening coeffs struct (?)
        outFlanges[i].absorbers.push_back(flStrAbsorber1);
        flStrAbsorber2.broadeningCoefficents.push_back({flStrAbsorber1.moleculeName, ind_coeff21});
        outFlanges[i].absorbers.push_back(flStrAbsorber2);

        if( flMatxGases.size() )
        {
            std::vector<StructAbsorber> flStrAbsorbersVec(flMatxGases.size());

            for(unsigned j=0; j<flStrAbsorbersVec.size(); j++)
            {
                bool loopErrorStatus = false;
                broadeningCoeffsFile.clear();
                broadeningCoeffsFile.seekg(0, ios::beg);

                // Ignore first two lines of INI file
                getline(broadeningCoeffsFile, lineFromFile);
                getline(broadeningCoeffsFile, lineFromFile);

                flStrAbsorbersVec[j].moleculeName = flMatxGases[j];
                flStrAbsorbersVec[j].concentration_ppm = flMatxConcs[j];

                // broadening...
                while( getline(broadeningCoeffsFile, lineFromFile) )
                {
                    loopErrorStatus = false;

                    std::istringstream ss(lineFromFile);
                    gasName1.clear();
                    gasName2.clear();
                    gasName3.clear();

                    ss >> gasName3;

                    if( gasName3 == flStrAbsorbersVec[j].moleculeName )
                    {
                        ss >> gasName1;

                        if( gasName1 == flStrAbsorber1.moleculeName )
                            ss >> ind_coeff31;
                        else if( gasName1 == flStrAbsorber2.moleculeName )
                            ss >> ind_coeff32;
                        else
                            std::cerr << "Error! Broadening coefficient for mixture of " << flStrAbsorbersVec[j].moleculeName
                                      << " with " << flStrAbsorber1.moleculeName << " and " << flStrAbsorber2.moleculeName
                                      << " not found." << std::endl;

                        ss.ignore(128,'\t');
                        ss >> gasName2;

                        if( gasName2 == flStrAbsorber1.moleculeName )
                            ss >> ind_coeff31;
                        else if( gasName2 == flStrAbsorber2.moleculeName )
                            ss >> ind_coeff32;
                        else
                            std::cerr << "Error! Broadening coefficient for mixture of " << flStrAbsorbersVec[j].moleculeName
                                      << " with " << flStrAbsorber1.moleculeName << " and " << flStrAbsorber2.moleculeName
                                      << " not found." << std::endl;

                        break;
                    }

                    loopErrorStatus = true;
                }

                if( loopErrorStatus )
                    std::cerr << "Error! Name of gas: " << flStrAbsorbersVec[j].moleculeName
                              << " not found in AllBroadeningCoefficients.ini!" << std::endl;

                flStrAbsorbersVec[j].broadeningCoefficents.push_back({flStrAbsorber1.moleculeName, ind_coeff31}); // To be created from broadening coeffs struct (?)
                flStrAbsorbersVec[j].broadeningCoefficents.push_back({flStrAbsorber2.moleculeName, ind_coeff32});
                outFlanges[i].absorbers.push_back(flStrAbsorbersVec[j]);
            }
        }
    }

    broadeningCoeffsFile.close();
}

std::string makeOutFileName(const std::string & spectraFilePath, const std::string & suffix)
{
    std::size_t indxStart = spectraFilePath.find_last_of('/');
    std::size_t indxStop = spectraFilePath.find_last_of('.');
    std::string fname = spectraFilePath.substr(indxStart + 1, indxStop - indxStart - 1) + "_" + suffix;
    return fname;
}

void printSpectralThings(const SPCInterface * spcInterface)
{

    std::cout << "Raw signal size " << spcInterface->getRawSignal(0).size()
        << " samplePts size " << spcInterface->getSamplePts(0).size() << "\n";
//    rawSignal is the ringdown appended to the data.  If they were not
//    contiguous then you will really get two separate data chunks
//    slapped together!
    std::cout << "rawSignal = " << spcInterface->getRawSignal(0) << "\n";
    std::cout << "samplePts = " << spcInterface->getSamplePts(0) << "\n";
    std::cout << "\n";

    std::vector<double> baselineFit = spcInterface->getBaselineFit(0);
    unsigned int ibl0 = spcInterface->getBaselineFitStart(0);

    std::cout << "baselineFit = " << baselineFit << "\n";
    std::cout << "ibl0 = range(" << ibl0 << "," << ibl0 + baselineFit.size() << ")\n";
    std::cout << "\n";
    std::cout << "measured = " << spcInterface->getMeasuredData(0) << "\n";
    std::cout << "peakFit = " << spcInterface->getPeakFit(0) << "\n";
    std::cout << "ilr0 = " << spcInterface->getLaserRampStartIdx() << "\n";
    std::cout << "nlr = " << spcInterface->getLaserRampLength() << "\n";
    std::cout << "\n";
    std::cout << "ghz = " << spcInterface->getGHzPts(0) << "\n";
}


void printPostFitThings(const SPCInterface* spcInterface)
{
        std::vector<double> rawSignal = spcInterface->getRawSignal(0);
//        std::cout << "rawSignal.size(): " << rawSignal.size() << std::endl;
        std::cout << "rawSignal = " << rawSignal << ";\n";

        std::vector<double> baselineFit = spcInterface->getBaselineFit(0);
//        std::cout << "baselineFit.size(): " << baselineFit.size() << std::endl;
        std::cout << "baselineFit = " << baselineFit << ";\n";

        std::vector<double> ringdownFit = spcInterface->getRingdownFit(0);
//        std::cout << "ringdownFit.size(): " << ringdownFit.size() << std::endl;

        std::vector<unsigned int> samplePts = spcInterface->getSamplePts(0);
//        std::cout << "samplePts.size(): " << samplePts.size() << std::endl;

        unsigned int baselineFitStart = spcInterface->getBaselineFitStart(0);
        std::cout << "baselineFitStart = " << baselineFitStart << ";\n";

        unsigned int ringDownFitStart = spcInterface->getRingdownFitStart(0);
        std::cout << "ringDownFitStart = " << ringDownFitStart << ";\n";

        std::vector<double> measuredData = spcInterface->getMeasuredData(0);
//        std::cout << "measuredData.size(): " << measuredData.size() << std::endl;

        std::vector<double> peakFit = spcInterface->getPeakFit(0);
//        std::cout << "peakFit.size(): " << peakFit.size() << std::endl;
        std::cout << "peakFit = " << peakFit << ";\n";

        std::vector<double> ghzPts = spcInterface->getGHzPts(0);
//        std::cout << "ghzPts.size(): " << ghzPts.size() << std::endl;
        std::cout << "ghzPts = " << ghzPts << ";\n";
}

///******************************************************************************************************************************************///
