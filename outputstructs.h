#ifndef OUTPUTSTRUCTS_H
#define OUTPUTSTRUCTS_H

#include <string>

struct OUTPUT_VARIABLES_STRUCT
{
    // Conditions within limits
    bool within_limits;
    std::string out_of_limits;

    // Collimation
    std::string collimation;

    // Insertion Tube Requirement
    bool fit_ins_tube;
    bool ins_tube;
    double L_ins_tube;

    // Insertion Tube Temperature
    double T_ins_tube_min;
    double T_ins_tube_typ;
    double T_ins_tube_max;

    // External Cell Length
    double L_ext_cell;

    // IR Blocking Filter
    bool Filter;

    // Spectral Scan Rate
    double Scan_Rate;

    // Outputs for linelocking
    bool TxRx_LL;
    bool Stack_LL;
    bool Flange_LL;
    bool ExtCell_LL;
    std::string LL_method;

    // Influence of Indirect Cross Interference outputs
    double acc_matrix_max_air_fix;
    double acc_matrix_max_N2_fix;
    double acc_matrix_max_cust_fix;
    double acc_matrix_max_air_float;
    double acc_matrix_max_N2_float;
    double acc_matrix_max_cust_float;
    double acc_matrix_min_air_fix;
    double acc_matrix_min_N2_fix;
    double acc_matrix_min_cust_fix;
    double acc_matrix_min_air_float;
    double acc_matrix_min_N2_float;
    double acc_matrix_min_cust_float;

    // Pressure Dependance outputs
    double acc_press_max_air_fix;
    double acc_press_max_N2_fix;
    double acc_press_max_cust_fix;
    double acc_press_max_air_float;
    double acc_press_max_N2_float;
    double acc_press_max_cust_float;
    double acc_press_min_air_fix;
    double acc_press_min_N2_fix;
    double acc_press_min_cust_fix;
    double acc_press_min_air_float;
    double acc_press_min_N2_float;
    double acc_press_min_cust_float;
    bool P_sensor_air_fix;
    bool P_sensor_N2_fix;
    bool P_sensor_cust_fix;
    bool P_sensor_air_float;
    bool P_sensor_N2_float;
    bool P_sensor_cust_float;

    // Temperature Dependance outputs
    double acc_temp_max_air_fix;
    double acc_temp_max_N2_fix;
    double acc_temp_max_cust_fix;
    double acc_temp_max_air_float;
    double acc_temp_max_N2_float;
    double acc_temp_max_cust_float;
    double acc_temp_min_air_fix;
    double acc_temp_min_N2_fix;
    double acc_temp_min_cust_fix;
    double acc_temp_min_air_float;
    double acc_temp_min_N2_float;
    double acc_temp_min_cust_float;
    bool T_sensor_air_fix;
    bool T_sensor_N2_fix;
    bool T_sensor_cust_fix;
    bool T_sensor_air_float;
    bool T_sensor_N2_float;
    bool T_sensor_cust_float;

    // Precision
    double prec_air_fix;
    double prec_N2_fix;
    double prec_cust_fix;
    double prec_air_float;
    double prec_N2_float;
    double prec_cust_float;

    // Lowest Detection Limit
    double LDL_air_fix;
    double LDL_N2_fix;
    double LDL_cust_fix;
    double LDL_air_float;
    double LDL_N2_float;
    double LDL_cust_float;

    // RAM Error
    double RAM_air_fix;
    double RAM_N2_fix;
    double RAM_cust_fix;
    double RAM_air_float;
    double RAM_N2_float;
    double RAM_cust_float;

    // Validation/Adjustment
    double IntCell_SNR_air_fix;
    double IntCell_SNR_N2_fix;
    double IntCell_SNR_cust_fix;

    // Transmission Outputs
    double Tr_orig_min;
    double Tr_orig_typ;
    double Tr_orig_max;
    double Tr_IT_min;
    double Tr_IT_typ;
    double Tr_IT_max;

    // Fixed or Floated PW
    bool fixed_only;

    OUTPUT_VARIABLES_STRUCT() : L_ins_tube(0.0), L_ext_cell(0.0), TxRx_LL(false), Stack_LL(false), Flange_LL(false), ExtCell_LL(false),
      acc_press_max_air_fix(-1.0), acc_press_max_N2_fix(-1.0), acc_press_max_cust_fix(-1.0), acc_press_max_air_float(-1.0),
      acc_press_max_N2_float(-1.0), acc_press_max_cust_float(-1.0), acc_press_min_air_fix(-1.0), acc_press_min_N2_fix(-1.0),
      acc_press_min_cust_fix(-1.0), acc_press_min_air_float(-1.0), acc_press_min_N2_float(-1.0), acc_press_min_cust_float(-1.0),
      acc_temp_max_air_fix(-1.0), acc_temp_max_N2_fix(-1.0), acc_temp_max_cust_fix(-1.0), acc_temp_max_air_float(-1.0),
      acc_temp_max_N2_float(-1.0), acc_temp_max_cust_float(-1.0), acc_temp_min_air_fix(-1.0), acc_temp_min_N2_fix(-1.0),
      acc_temp_min_cust_fix(-1.0), acc_temp_min_air_float(-1.0), acc_temp_min_N2_float(-1.0), acc_temp_min_cust_float(-1.0),
      fixed_only(true)
    {}
};

#endif // OUTPUTSTRUCTS_H
