# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration:
Windows + MinGW 5.3.0 (32bit) + Qt 5

* Dependencies:
ABB libMASS library (check INCLUDEPATH in .pro file)

* Database configuration:
for standard spectra fitting tasks (Spectrum Plotter tab) put your txt files in /Spectra directory and ini files in /INI_Beta03_FromFRA directory;
for spectra simulation (Spectrum Generator tab) you need crossstack.ini, phys_consts.ini and input.ini in /MasterINI directory; crossstack.ini with
additional peaks and phys_consts.ini should also be put in /FitINI directory.

* How to run tests:
no automatic test yet, use UI

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin:
Bartek Kozłowiec (Mobica)

* Other community or team contact:
Dmitry Skvortsov (ABB)