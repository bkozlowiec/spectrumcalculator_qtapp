#ifndef O2CONFIGURATIONSTRUCTS_H
#define O2CONFIGURATIONSTRUCTS_H

#include <vector>
#include <string>
#include <iostream>

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct O2CONFIG_COMMON_STRUCT
{
    const double   intCellLenOpt;      // Internal cell length [mm]

    const double   purgeVel;        // Purge gas flow velocity [m/s]
    const double   partVelMax;      // Maximum particle velocity [m/s]

    const double   collimXlen;      // Collimation crossover length [mm]
    const double   filterXtemp;     // IR blocking filter crossover temp [deg C]
    const double   scanXpres;       // Scan Rate crossover pressure [bar]

    const double   transmMin;       // Minimum transmission [%]
    const unsigned SNRllMin;        // Minimum Signal to Noise Ratio for linelock [-]

    const double   TxLen;           // length of air gap in Tx [mm]
    const double   RxLen;           // length of air gap in Rx [mm]

    const double   PresAtm;         // Atmospheric pressure [bar]
    const double   conc1xAir;       // Concentration of measurement component 1 in air [ppm]

    const double   OPLenMin;        // Minimum optical path length [mm]
    const double   OPLenMax;        // Maximum optical path length [mm]

    const double   ATLimMin;        // Minimum ambient temperature limit [deg C]
    const double   ATLimMax;        // Maximum ambient temperature limit [deg C]

    const double   rangeLimMin;     // Minimum measurement range limit [%]
    const double   rangeLimMax;     // Maximum measurement range limit [%]

    const double   PPlimMin;        // Minimum pressure limit [bar]
    const double   PPlimMax;        // Maximum pressure limit [bar]

    const double   PTlimMin;        // Minimum temperature limit [deg C]
    const double   PTlimMax;        // Maximum temperature limit [deg C]

    const double   specRep;         // Repeatability spec [%]
    const double   specLin;         // Linearity spec [%]

    const double   specTdep;        // Temperature dependence without compensation [%/K]
    const double   specPdep;        // Pressure dependence without compensation [%/hPa]

    const double   threshICI;       // Matrix uncertainty threshold [% of FS]
    const double   conc1xIntCell;   // Internal cell measurement component concentration [ppm]

    // The default and only constructor
    O2CONFIG_COMMON_STRUCT() :
        intCellLenOpt(40.0),
        purgeVel(1.0),
        partVelMax(25.0),
        collimXlen(6000.0),
        filterXtemp(600.0),
        scanXpres(0.8),
        transmMin(2.5),
        SNRllMin(10),
        TxLen(4.0),
        RxLen(4.0),
        PresAtm(1.011),
        conc1xAir(2.1e5),
        OPLenMin(300.0),
        OPLenMax(20000.0),
        ATLimMin(-20.0),
        ATLimMax(55.0),
        rangeLimMin(1.0),
        rangeLimMax(100.0),
        PPlimMin(0.03),
        PPlimMax(20.0),
        PTlimMin(-100.0),
        PTlimMax(1500.0),
        specRep(0.2),
        specLin(1.0),
        specTdep(0.2),
        specPdep(0.1),
        threshICI(10.0),
        conc1xIntCell(1.0e6)
    {}

    // The virtual destructor
    virtual ~O2CONFIG_COMMON_STRUCT() {}
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct O2CONFIG_OPTION1_STRUCT : public O2CONFIG_COMMON_STRUCT
{
    const double   extCellLenOpt;      // External cell length [mm]
    const double   isoFlangeLenOpt;    // Isolation flange length [mm]
    const double   insTubeLenOpt;      // Insertion tube length [mm]

    // The default and only constructor
    O2CONFIG_OPTION1_STRUCT() :
        extCellLenOpt(500.0),
        isoFlangeLenOpt(33.0),
        insTubeLenOpt(978.0)
    {}
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct O2CONFIG_OPTION2_STRUCT : public O2CONFIG_COMMON_STRUCT
{
    const double   extCellLenOpt;      // External cell length [mm]
    const double   isoFlangeLenOpt;    // Isolation flange length [mm]
    const double   insTubeLenOpt;      // Insertion tube length [mm]

    // The default and only constructor
    O2CONFIG_OPTION2_STRUCT() :
        extCellLenOpt(1000.0),
        isoFlangeLenOpt(33.0),
        insTubeLenOpt(1000.0)
    {}
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct O2CONFIG_OPTION3_STRUCT : public O2CONFIG_COMMON_STRUCT
{
    const double   extCellLenOpt;      // External cell length [mm]
    const double   isoFlangeLenOpt;    // Isolation flange length [mm]
    const double   insTubeLenOpt;      // Insertion tube length [mm]

    // The default and only constructor
    O2CONFIG_OPTION3_STRUCT() :
        extCellLenOpt(500.0),
        isoFlangeLenOpt(1000.0),
        insTubeLenOpt(1000.0)
    {}
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct GASCONFIG_VARIABLES_STRUCT
{
    double   extCellLenOpt;      // External cell length [mm]
    double   intCellLenOpt;      // Internal cell length [mm]
    double   isoFlangeLenOpt;    // Isolation flange length [mm]
    double   insTubeLenOpt;      // Insertion tube length [mm]

    double   purgeVel;        // Purge gas flow velocity [m/s]
    double   partVelMax;      // Maximum particle velocity [m/s]

    double   collimXlen;      // Collimation crossover length [mm]
    double   filterXtemp;     // IR blocking filter crossover temp [deg C]
    double   scanXpres;       // Scan Rate crossover pressure [bar]

    double   transmMin;       // Minimum transmission [%]
    unsigned SNRllMin;        // Minimum Signal to Noise Ratio for linelock [-]

    double   TxLen;           // length of air gap in Tx [mm]
    double   RxLen;           // length of air gap in Rx [mm]

    double   PresAtm;         // Atmospheric pressure [bar]
    double   conc1xAir;       // Concentration of measurement component 1 in air [ppm]

    double   OPLenMin;        // Minimum optical path length [mm]
    double   OPLenMax;        // Maximum optical path length [mm]

    double   ATLimMin;        // Minimum ambient temperature limit [deg C]
    double   ATLimMax;        // Maximum ambient temperature limit [deg C]

    double   rangeLimMin;     // Minimum measurement range limit [%]
    double   rangeLimMax;     // Maximum measurement range limit [%]

    double   PPLimMin;        // Minimum pressure limit [bar]
    double   PPLimMax;        // Maximum pressure limit [bar]

    double   PTLimMin;        // Minimum temperature limit [deg C]
    double   PTLimMax;        // Maximum temperature limit [deg C]

    double   specRep;         // Repeatability spec [%]
    double   specLin;         // Linearity spec [%]

    double   specTdep;        // Temperature dependence without compensation [%/K]
    double   specPdep;        // Pressure dependence without compensation [%/hPa]

    double   threshICI;       // Matrix uncertainty threshold [% of FS]
    double   conc1xIntCell;   // Internal cell measurement component concentration [ppm]

    // Constructor
    GASCONFIG_VARIABLES_STRUCT( O2CONFIG_OPTION1_STRUCT defaults1 = O2CONFIG_OPTION1_STRUCT() ) :
        extCellLenOpt( defaults1.extCellLenOpt ),
        intCellLenOpt( defaults1.intCellLenOpt ),
        isoFlangeLenOpt( defaults1.isoFlangeLenOpt ),
        insTubeLenOpt( defaults1.insTubeLenOpt ),
        purgeVel( defaults1.purgeVel ),
        partVelMax( defaults1.partVelMax ),
        collimXlen( defaults1.collimXlen ),
        filterXtemp( defaults1.filterXtemp ),
        scanXpres( defaults1.scanXpres ),
        transmMin( defaults1.transmMin ),
        SNRllMin( defaults1.SNRllMin ),
        TxLen( defaults1.TxLen ),
        RxLen( defaults1.RxLen ),
        PresAtm( defaults1.PresAtm ),
        conc1xAir( defaults1.conc1xAir ),
        OPLenMin( defaults1.OPLenMin ),
        OPLenMax( defaults1.OPLenMax ),
        ATLimMin( defaults1.ATLimMin ),
        ATLimMax( defaults1.ATLimMax ),
        rangeLimMin( defaults1.rangeLimMin ),
        rangeLimMax( defaults1.rangeLimMax ),
        PPLimMin( defaults1.PPlimMin ),
        PPLimMax( defaults1.PPlimMax ),
        PTLimMin( defaults1.PTlimMin ),
        PTLimMax( defaults1.PTlimMax ),
        specRep( defaults1.specRep ),
        specLin( defaults1.specLin ),
        specTdep( defaults1.specTdep ),
        specPdep( defaults1.specPdep ),
        threshICI( defaults1.threshICI ),
        conc1xIntCell( defaults1.conc1xIntCell )
    {}

    // Assignment operator
    GASCONFIG_VARIABLES_STRUCT& operator=( O2CONFIG_COMMON_STRUCT* defPtr )
    {
        intCellLenOpt = defPtr->intCellLenOpt;
        purgeVel = defPtr->purgeVel;
        partVelMax = defPtr->partVelMax;
        collimXlen = defPtr->collimXlen;
        filterXtemp = defPtr->filterXtemp;
        scanXpres = defPtr->scanXpres;
        transmMin = defPtr->transmMin;
        SNRllMin = defPtr->SNRllMin;
        TxLen = defPtr->TxLen;
        RxLen = defPtr->RxLen;
        PresAtm = defPtr->PresAtm;
        conc1xAir = defPtr->conc1xAir;
        OPLenMin = defPtr->OPLenMin;
        OPLenMax = defPtr->OPLenMax;
        ATLimMin = defPtr->ATLimMin;
        ATLimMax = defPtr->ATLimMax;
        rangeLimMin = defPtr->rangeLimMin;
        rangeLimMax = defPtr->rangeLimMax;
        PPLimMin = defPtr->PPlimMin;
        PPLimMax = defPtr->PPlimMax;
        PTLimMin = defPtr->PTlimMin;
        PTLimMax = defPtr->PTlimMax;
        specRep = defPtr->specRep;
        specLin = defPtr->specLin;
        specTdep = defPtr->specTdep;
        specPdep = defPtr->specPdep;
        threshICI = defPtr->threshICI;
        conc1xIntCell = defPtr->conc1xIntCell;

        if( O2CONFIG_OPTION1_STRUCT* defOpt1 = dynamic_cast<O2CONFIG_OPTION1_STRUCT*>(defPtr) ) {
            extCellLenOpt   = defOpt1->extCellLenOpt;
            isoFlangeLenOpt = defOpt1->isoFlangeLenOpt;
            insTubeLenOpt   = defOpt1->insTubeLenOpt;
        }
        else if( O2CONFIG_OPTION2_STRUCT* defOpt2 = dynamic_cast<O2CONFIG_OPTION2_STRUCT*>(defPtr) ) {
            extCellLenOpt   = defOpt2->extCellLenOpt;
            isoFlangeLenOpt = defOpt2->isoFlangeLenOpt;
            insTubeLenOpt   = defOpt2->insTubeLenOpt;
        }
        else if( O2CONFIG_OPTION3_STRUCT* defOpt3 = dynamic_cast<O2CONFIG_OPTION3_STRUCT*>(defPtr) ) {
            extCellLenOpt   = defOpt3->extCellLenOpt;
            isoFlangeLenOpt = defOpt3->isoFlangeLenOpt;
            insTubeLenOpt   = defOpt3->insTubeLenOpt;
        }
        else
            std::cerr << "Pointer to O2CONFIG_COMMON_STRUCT badly casted!" << std::endl;

        return *this;
    }
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct GASCONFIG_SIM_FIT_STRUCT
{
    bool useSimFitStruct;

    // Enumeration type for creating gas matrix
    enum mtxCreationStyles { basic, minPW, maxPW };

    // True if stack fitting depends on simulated concentration value X1
    bool stackFittingLater;

    // Common for stack, flanges, simulation as well as fitting data
    std::string nameX1, nameX2;           // Main gases names

    //////////////////////
    struct INPUT_DATA
    {
        struct chamberInfo
        {
            INPUT_DATA* parentPtr;
            double X1, X2, T, P, L;             // #1 and #2 main gas concentrations [ppm], Stack temperature [deg C],
                                                // Stack pressure [bar], Stack path length [mm]
            std::vector<double> matrX;          // Matrix gases concentrations [ppm]
            std::vector<std::string> nameMatrX; // Matrix gases names
            mtxCreationStyles mtxStyle;

            chamberInfo( INPUT_DATA* parent ) : parentPtr(parent), mtxStyle(basic) {}
            virtual void clear() = 0;
        };

        struct stackInfo : chamberInfo
        {
            bool isEmpty;
            stackInfo( INPUT_DATA* parent ) : chamberInfo(parent), isEmpty(true) {}
            void clear() { matrX.clear(); nameMatrX.clear(); isEmpty = true; mtxStyle = basic; }
        };

        struct flangeInfo : chamberInfo
        {
            bool isInsTube;
            flangeInfo( INPUT_DATA* parent ) : chamberInfo(parent), isInsTube(false) {}
            void clear() { matrX.clear(); nameMatrX.clear(); isInsTube = false; mtxStyle = basic; }
        };

        virtual void clear() = 0;
    };

    struct SIM_DATA : INPUT_DATA
    {
        stackInfo stack;
        std::vector<flangeInfo> flanges;

        SIM_DATA() : stack(this) {}
        void clear() { stack.clear(); flanges.clear(); }
    };

    struct FIT_DATA : INPUT_DATA
    {
        stackInfo stack;
        std::vector<flangeInfo> flanges;

        FIT_DATA() : stack(this) {}
        void clear() { stack.clear(); flanges.clear(); }
    };

    SIM_DATA simInput;
    FIT_DATA fitInput;
    void newClear() { nameX1.clear(); nameX2.clear(); simInput.clear(); fitInput.clear(); }
    //////////////////////

    struct GASCONFIG_SIM_FIT_INPUT
    {
        // Flanges initialization variables
        struct chamberInfo
        {
            double X1, X2, T, P, L;             // #1 and #2 main gas concentrations [ppm], Stack temperature [deg C],
                                                // Stack pressure [bar], Stack path length [mm]
            std::vector<double> matrX;          // Matrix gases concentrations [ppm]
            std::vector<std::string> nameMatrX; // Matrix gases names
            mtxCreationStyles mtxStyle;

            chamberInfo() : mtxStyle(basic) {}
            void clear() { matrX.clear(); nameMatrX.clear(); mtxStyle = basic; }
        };

        struct stackInfo : chamberInfo
        {
            bool isEmpty;
            stackInfo() : isEmpty(true) {}
            void clear() { isEmpty = true; chamberInfo::clear(); }
        };

        struct flangeInfo : chamberInfo
        {
            bool isInsTube;
            flangeInfo() : isInsTube(false) {}
            void clear() { isInsTube = false; chamberInfo::clear(); }
        };

        stackInfo stack;                   // Stack initialization variables
        std::vector<flangeInfo> flanges;   // Flanges initialization variables

        void clear() { stack.clear(); flanges.clear(); }
    };

    GASCONFIG_SIM_FIT_INPUT simData, fitData;

    GASCONFIG_SIM_FIT_STRUCT() : useSimFitStruct(false), stackFittingLater(false) {}
    void clear() { nameX1.clear(); nameX2.clear(); simData.clear(); fitData.clear(); }

    void printInnerts() { std::cerr << "\n***** SIMFITDATA INNERTS ****\n"
                                   << "useSimFitStruct: " << useSimFitStruct << "\n"
                                   << "stackFittingLater: " << stackFittingLater << "\n"
                                   << "nameX1: " << nameX1 << "\n"
                                   << "nameX2: " << nameX2 << "\n"
                                   << "simData.stack.X1 = " << simData.stack.X1 << "\n"
                                   << "simData.stack.X2 = " << simData.stack.X2 << "\n"
                                   << "simData.stack.T  = " << simData.stack.T << "\n"
                                   << "simData.stack.P  = " << simData.stack.P << "\n"
                                   << "simData.stack.L  = " << simData.stack.L << "\n"
                                   << "simData.stack.mtxStyle = " << simData.stack.mtxStyle << "\n"
                                   << "simData.stack.isEmpty = " << simData.stack.isEmpty << "\n";
                         for(unsigned i=0; i<simData.stack.matrX.size(); ++i)
                             std::cerr << "simData.stack.matrX[" << i << "] = " << simData.stack.matrX[i] << "\n";
                         for(unsigned i=0; i<simData.stack.nameMatrX.size(); ++i)
                             std::cerr << "simData.stack.nameMatrX[" << i << "] = " << simData.stack.nameMatrX[i] << "\n";

                                          std::cerr << "\nfitData.stack.X1 = " << fitData.stack.X1 << "\n"
                                                                             << "fitData.stack.X2 = " << fitData.stack.X2 << "\n"
                                                                             << "fitData.stack.T  = " << fitData.stack.T << "\n"
                                                                             << "fitData.stack.P  = " << fitData.stack.P << "\n"
                                                                             << "fitData.stack.L  = " << fitData.stack.L << "\n"
                                                                             << "fitData.stack.mtxStyle = " << fitData.stack.mtxStyle << "\n"
                                                                             << "fitData.stack.isEmpty = " << fitData.stack.isEmpty << "\n";
                                                                   for(unsigned i=0; i<fitData.stack.matrX.size(); ++i)
                                                                       std::cerr << "fitData.stack.matrX[" << i << "] = " << fitData.stack.matrX[i] << "\n";
                                                                   for(unsigned i=0; i<fitData.stack.nameMatrX.size(); ++i)
                                                                       std::cerr << "fitData.stack.nameMatrX[" << i << "] = " << fitData.stack.nameMatrX[i] << "\n\n";

                                                                                    for(unsigned i=0; i<simData.flanges.size(); ++i)
                                                                                    {
                                                                                                                  std::cerr << "simData.flange.X1 = " << simData.flanges[i].X1 << "\n"
                                                                                                                           << "simData.flange.X2 = " << simData.flanges[i].X2 << "\n"
                                                                                                                           << "simData.flange.T  = " << simData.flanges[i].T << "\n"
                                                                                                                           << "simData.flange.P  = " << simData.flanges[i].P << "\n"
                                                                                                                           << "simData.flange.L  = " << simData.flanges[i].L << "\n"
                                                                                                                           << "simData.flange.mtxStyle = " << simData.flanges[i].mtxStyle << "\n";
                                                                                                                 for(unsigned ii=0; ii<simData.flanges[i].matrX.size(); ++ii)
                                                                                                                     std::cerr << "simData.flange.matrX[" << ii << "] = " << simData.flanges[i].matrX[ii] << "\n";
                                                                                                                 for(unsigned ii=0; ii<simData.flanges[i].nameMatrX.size(); ++ii)
                                                                                                                     std::cerr << "simData.flange.nameMatrX[" << ii << "] = " << simData.flanges[i].nameMatrX[ii] << "\n\n";
                                                                                    }

                                                                                                     for(unsigned i=0; i<fitData.flanges.size(); ++i)
                                                                                                     {
                                                                                                                                   std::cerr << "fitData.flange.X1 = " << fitData.flanges[i].X1 << "\n"
                                                                                                                                            << "fitData.flange.X2 = " << fitData.flanges[i].X2 << "\n"
                                                                                                                                            << "fitData.flange.T  = " << fitData.flanges[i].T << "\n"
                                                                                                                                            << "fitData.flange.P  = " << fitData.flanges[i].P << "\n"
                                                                                                                                            << "fitData.flange.L  = " << fitData.flanges[i].L << "\n"
                                                                                                                                            << "fitData.flange.mtxStyle = " << fitData.flanges[i].mtxStyle << "\n";
                                                                                                                                  for(unsigned ii=0; ii<fitData.flanges[i].matrX.size(); ++ii)
                                                                                                                                      std::cerr << "fitData.flange.matrX[" << ii << "] = " << fitData.flanges[i].matrX[ii] << "\n";
                                                                                                                                  for(unsigned ii=0; ii<fitData.flanges[i].nameMatrX.size(); ++ii)
                                                                                                                                      std::cerr << "fitData.flange.nameMatrX[" << ii << "] = " << fitData.flanges[i].nameMatrX[ii] << "\n\n";
                                                                                                     }
                       }
};

#endif // O2CONFIGURATIONSTRUCTS_H
