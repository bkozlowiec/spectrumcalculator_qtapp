#-------------------------------------------------
#
# Project created by QtCreator 2017-08-16T17:19:46
#
#-------------------------------------------------

QT       += core gui
CONFIG   += qwt c++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SpectrumCalculator
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        widget.cpp \
    spcgenerator.cpp \
    noisegenerator.cpp \
    qtspcplotter.cpp \
    dialog.cpp \
    qtspcgenerator.cpp \
    mtxdialog.cpp

HEADERS += \
        widget.h \
    spcgenerator.h \
    noisegenerator.h \
    qtspcgenerator.h \
    qtspcplotter.h \
    dialog.h \
    inputstructs.h \
    mtxdialog.h \
    outputstructs.h \
    gasconfigstructs.h

FORMS += \
        widget.ui \
    dialog.ui \
    mtxdialog.ui

INCLUDEPATH += "C:/DEV/libMASS_2+/Src"

# ATW SPC specific includes
ATW_SPC = C:/DEV/

CONFIG(debug, debug|release) {
    LIBS += $$ATW_SPC/build-libMASS_2+-Desktop_Qt_5_9_0_MinGW_32bit-Debug/Src/libMASS.a
    message("Building 32 bit debug")
}

CONFIG(release, debug|release) {
    LIBS += $$ATW_SPC/build-libMASS_2+-Desktop_Qt_5_9_0_MinGW_32bit-Release/Src/libMASS.a
    message("Building 32 bit release")
}
