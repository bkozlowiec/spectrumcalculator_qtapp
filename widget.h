#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QDir>
#include <vector>
#include <string>
#include "dialog.h"
#include "mtxdialog.h"

class QwtPlot;
class Baseline;
class qtSpcPlotter;
class qtSpcGenerator;
class qtPlotterThread;
class qtGeneratorThread;
class qtDllThread;
class Dialog;
class MtxDialog;

//-------------------------Global function----------------------------

void linspace( std::vector<double>&, double, double, unsigned );

//--------------------------------------------------------------------

namespace Ui {
class Widget;
}

//--------------------------------------------------------------------

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget* parent = 0);
    ~Widget();

    void setXpoints();
    void calcYpoints();
    void createPlotL();
    void createPlotR();
    void createPlotT3();
    void drawBaseline();
    void drawPlotterOutputL();
    void drawPlotterOutputR();
    void drawGeneratorOutputSC();
    void drawGeneratorOutputMC();
    void colorLCDnums();
    void setSimFitConfiguration();

public slots:
    void drawPlotterOutput();
    void drawGeneratorOutput();
    void receiveDialogMsg( multipleCalcsData );

private slots:
    void on_pushButton_1_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_4_clicked();
    void on_pushButton_5_clicked();
    void on_pushButton_6_clicked();
    void on_pushButton_7_clicked();
    void on_pushButton_8_clicked();
    void on_pushButton_9_clicked();
    void on_pushButton_10_clicked();

    void if_comboBox1_changed( QString );
    void if_comboBox2_changed( QString );
    void if_comboBox3_changed( QString );
    void if_comboBox4_changed( QString );
    void if_comboBox21_changed( QString );
    void if_comboBox41_changed( QString );
    void if_comboBox42_changed( QString );
    void if_comboBox43_changed( QString );
    void if_comboBox26_changed( QString );
    void if_comboBox_changed( int );

    void on_radioButton_toggled( bool );
    void turnSimConfigOff();

    void calculateAvailableOutput();

    void setPsensorAirFixedOutput( QString );
    void setPsensorN2FixedOutput( QString );
    void setPsensorCustFixedOutput( QString );
    void setPsensorAirFloatedOutput( QString );
    void setPsensorN2FloatedOutput( QString );
    void setPsensorCustFloatedOutput( QString );

    void setTsensorAirFixedOutput( QString );
    void setTsensorN2FixedOutput( QString );
    void setTsensorCustFixedOutput( QString );
    void setTsensorAirFloatedOutput( QString );
    void setTsensorN2FloatedOutput( QString );
    void setTsensorCustFloatedOutput( QString );

    void afterDllThreadFinished();

private:
    Ui::Widget* ui;
    QwtPlot* plotL;
    QwtPlot* plotR;
    QVector<QwtPlot*> plotT3;

    qtSpcPlotter* spcPtr;
    qtSpcGenerator* spcGen;
    Baseline* blOld;
    Baseline* blFitted;
    qtPlotterThread* plotterThread;
    qtGeneratorThread* generatorThread;
    qtDllThread* dllThread;

    QDir projectDir;
    std::string fileName;
    std::string iniDirName;

    std::vector<double> xPoints; // frequency points [GHz]
    std::vector<double> yPtsOld, yPtsFitted; // absorbance points
    double xStart, xEnd;
    unsigned howManyPts;

    void setupPlotZooming( QwtPlot* );
    void setICIcheckConfig( int );
    void setPressureDepConfig( int );
    void setTemperatureDepConfig( int );
    void setPrecisionLDLconfig( int );
    void setRAMconfig( int );
    void setVAconfig( int );

public:
    qtSpcGenerator* getSpcGenerator() { return spcGen; }
};

#endif // WIDGET_H
