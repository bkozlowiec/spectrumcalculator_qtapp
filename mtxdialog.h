#ifndef MTXDIALOG_H
#define MTXDIALOG_H

#include <QDialog>

//--------------------------------------------------------------------------------

namespace Ui {
class MtxDialog;
}

//--------------------------------------------------------------------------------

class MtxDialog : public QDialog
{
    Q_OBJECT

    Ui::MtxDialog* ui;

private slots:
    void addGasComponent();
    void removeGasComponent();
    void possiblyEnableGasEditing();
    void loadGasProperties( QString );
    void importGasList();
    void checkConcentationConsistency();

public:
    explicit MtxDialog(QWidget* parent = 0);
    ~MtxDialog();

signals:
    void newGasNameSet( QString );
    void newGasConcTypSet( double );
    void newGasConcMinSet( double );
    void newGasConcMaxSet( double );
    void oldGasSet( QString );
};

#endif // MTXDIALOG_H
