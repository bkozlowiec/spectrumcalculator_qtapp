#ifndef QTSPCPLOTTER_H
#define QTSPCPLOTTER_H

#include <vector>
#include <string>
#include <QMutex>
#include <QThread>
#include <iostream>

////////////////////////////////////////////////////////////////////////////////////////////////

class qtSpcPlotter
{
    unsigned NumOfPoints;

    std::vector<double> ghzPoints;
    std::vector<double> rawSignal;
    std::vector<double> baselineFit;
    std::vector<double> measuredData;
    std::vector<double> peakFit;
    std::vector<double> initialGuessFit;
    std::vector<double> estimatedFit;
    std::vector<double> originalSig;
    std::vector<double> manualInitGuess;

    std::vector<double> oldBlParams;
    std::vector<double> newBlParams;

public:
    void emulateSPCtool(std::string, std::string);
    //void getPathNames(std::string s1, std::string s2) { fileName = s1; iniDirName = s2; }
    std::vector<double> computeMyInitGuess();

    unsigned getNumOfPoints() { return NumOfPoints; }
    const std::vector<double>& getGhzPoints() { return ghzPoints; }
    const std::vector<double>& getRawSignal() { return rawSignal; }
    const std::vector<double>& getBaselineFit() { return baselineFit; }
    const std::vector<double>& getMeasuredData() { return measuredData; }
    const std::vector<double>& getPeakFit() { return peakFit; }
    const std::vector<double>& getInitialGuessFit() { return initialGuessFit; }
    const std::vector<double>& getEstimatedFit() { return estimatedFit; }
    const std::vector<double>& getOriginalSig() { return originalSig; }
    const std::vector<double>& getManualInitGuess() { return manualInitGuess; }

    const std::vector<double>& getOldBlParams() { return oldBlParams; }
    const std::vector<double>& getNewBlParams() { return newBlParams; }
};

////////////////////////////////////////////////////////////////////////////////////////////////

class qtPlotterThread : public QThread
{
    Q_OBJECT

    qtSpcPlotter* spcPointer;
    QMutex mutex;

    std::string string1, string2;
    void run() {
        QMutexLocker locker(&mutex);
        spcPointer->emulateSPCtool(string1,string2);
        std::cout << "Hello from worker thread: " << thread()->currentThreadId() << std::endl;
        emit resultReady(); }

public:
    qtPlotterThread(qtSpcPlotter* spcPtr) : spcPointer(spcPtr) {}
    qtPlotterThread( const qtPlotterThread& ) = delete;
    qtPlotterThread& operator=( const qtPlotterThread& ) = delete;
    void getPathStrings(std::string fileName, std::string iniDirName) {
        string1 = fileName; string2 = iniDirName;
    }

signals:
    void resultReady();
};

#endif // QTSPCPLOTTER_H
